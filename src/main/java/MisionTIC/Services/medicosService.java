/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MisionTIC.Services;

import MisionTIC.Models.medicos;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author USUARIO
 */
public interface medicosService
{
    //AGREGAR O INSERTAR
    public medicos save(medicos medicos);
    //BORRAR
    public void delete(Integer IdMedico);
    //BUSCAR POR ID
    public medicos findById(Integer IdMedico);
    //BUSCAR TODOS
    public List<medicos> findAll();
    //login
    public ArrayList<medicos> findByCedula(Integer Cedula);
}
