package MisionTIC.Services;
import MisionTIC.Models.consultorios;
import java.util.List;

public interface consultoriosService
{
    //AGREGAR O INSERTAR
    public consultorios save(consultorios consultorio);
    //BORRAR
    public void delete(Integer IdConsultorio);
    //BUSCAR POR ID
    public consultorios findById(Integer IdConsultorio);
    //BUSCAR TODOS
    public List<consultorios> findAll();
}
