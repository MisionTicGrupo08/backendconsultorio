package MisionTIC.Services;
import MisionTIC.Models.diagnTienenMedicas;
import java.util.List;

public interface diagnTienenMedicasService
{
    //AGREGAR O INSERTAR
    public diagnTienenMedicas save(diagnTienenMedicas usuario);
    //BORRAR
    public void delete(Integer id);
    //BUSCAR POR ID
    public diagnTienenMedicas findById(Integer id);
    //BUSCAR TODOS
    public List<diagnTienenMedicas> findAll();
}
