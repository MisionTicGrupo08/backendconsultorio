package MisionTIC.Services;

import MisionTIC.Models.administradores;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author USUARIO
 */
public interface administradoresService
{
    //AGREGAR O INSERTAR
    public administradores save(administradores administradores);
    //BORRAR
    public void delete(Integer IdAdministradores);
    //BUSCAR POR ID
    public administradores findById(Integer IdAdministradores);
    //BUSCAR TODOS
    public List<administradores> findAll();
    //login
    public ArrayList<administradores> findByUsuario(String clave);
}
