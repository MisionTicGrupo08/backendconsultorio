package MisionTIC.Services;
import MisionTIC.Models.medicamentos;
import java.util.List;

public interface medicamentosService
{
    //AGREGAR O INSERTAR
    public medicamentos save(medicamentos usuario);
    //BORRAR
    public void delete(Integer id);
    //BUSCAR POR ID
    public medicamentos findById(Integer id);
    //BUSCAR TODOS
    public List<medicamentos> findAll();
}
