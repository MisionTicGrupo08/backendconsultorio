package MisionTIC.Services;
import MisionTIC.Models.citasAgendadas;
import java.util.List;

public interface citasAgendadasService
{
    //AGREGAR O INSERTAR
    public citasAgendadas save(citasAgendadas usuario);
    //BORRAR
    public void delete(Integer id);
    //BUSCAR POR ID
    public citasAgendadas findById(Integer id);
    //BUSCAR TODOS
    public List<citasAgendadas> findAll();
    //buscar por paciente
    
}
