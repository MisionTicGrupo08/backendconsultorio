package MisionTIC.Services;
import MisionTIC.Models.diagnosticos;
import java.util.List;

public interface diagnosticosService
{
    //AGREGAR O INSERTAR
    public diagnosticos save(diagnosticos usuario);
    //BORRAR
    public void delete(Integer id);
    //BUSCAR POR ID
    public diagnosticos findById(Integer id);
    //BUSCAR TODOS
    public List<diagnosticos> findAll();
}
