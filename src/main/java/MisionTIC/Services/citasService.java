/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MisionTIC.Services;

import MisionTIC.Models.citas;
import java.util.List;
/**
 *
 * @author USUARIO
 */
public interface citasService
{
    //AGREGAR O INSERTAR
    public citas save(citas citas);
    //BORRAR
    public void delete(Integer IdCita);
    //BUSCAR POR ID
    public citas findById(Integer IdCita);
    //BUSCAR TODOS
    public List<citas> findAll();
}
