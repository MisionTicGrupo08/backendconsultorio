package MisionTIC.Services;
import MisionTIC.Models.pacientes;

import java.util.ArrayList;
import java.util.List;

public interface pacientesService
{
    //AGREGAR O INSERTAR
    public pacientes save(pacientes usuario);
    //BORRAR
    public void delete(Integer id);
    //BUSCAR POR ID
    public pacientes findById(Integer id);
    //BUSCAR TODOS
    public List<pacientes> findAll();
    //buscar por numero de afiliado
    public ArrayList<pacientes> findByNumeroAfiliado(String numeroAfiliado);

}