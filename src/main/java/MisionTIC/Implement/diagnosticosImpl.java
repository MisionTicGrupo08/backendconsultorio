package MisionTIC.Implement;
import MisionTIC.Dao.diagnosticosDao;
import MisionTIC.Models.diagnosticos;
import MisionTIC.Services.diagnosticosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class diagnosticosImpl implements diagnosticosService
{
    @Autowired
    private diagnosticosDao diagnosticosDao;
    
    //AGREGAR - ACTUALIZAR
    @Override
    @Transactional(readOnly=false)
    public diagnosticos save(diagnosticos diagnostico)
    {
        return diagnosticosDao.save(diagnostico);
    }

    //BORRAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id)
    {
        diagnosticosDao.deleteById(id);
    }

    //BUSCAR POR ID
    @Override
    @Transactional(readOnly=true)
    public diagnosticos findById(Integer id)
    {
        return diagnosticosDao.findById(id).orElse(null);
    }

    //BUSCAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<diagnosticos> findAll()
    {
        return (List<diagnosticos>) diagnosticosDao.findAll();
    }

}