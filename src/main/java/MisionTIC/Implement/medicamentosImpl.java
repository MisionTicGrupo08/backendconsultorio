package MisionTIC.Implement;
import MisionTIC.Dao.medicamentosDao;
import MisionTIC.Models.medicamentos;
import MisionTIC.Services.medicamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class medicamentosImpl implements medicamentosService
{
    @Autowired
    private medicamentosDao medicamentosDao;
    
    //AGREGAR - ACTUALIZAR
    @Override
    @Transactional(readOnly=false)
    public medicamentos save(medicamentos medicamento)
    {
        return medicamentosDao.save(medicamento);
    }

    //BORRAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id)
    {
        medicamentosDao.deleteById(id);
    }

    //BUSCAR POR ID
    @Override
    @Transactional(readOnly=true)
    public medicamentos findById(Integer id)
    {
        return medicamentosDao.findById(id).orElse(null);
    }

    //BUSCAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<medicamentos> findAll()
    {
        return (List<medicamentos>) medicamentosDao.findAll();
    }

}