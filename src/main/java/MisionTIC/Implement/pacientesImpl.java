package MisionTIC.Implement;
import MisionTIC.Dao.pacientesDao;
import MisionTIC.Models.pacientes;
import MisionTIC.Services.pacientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class pacientesImpl implements pacientesService
{
    @Autowired
    private pacientesDao pacientesDao;
    
    //AGREGAR - ACTUALIZAR
    @Override
    @Transactional(readOnly=false)
    public pacientes save(pacientes paciente)
    {
        return pacientesDao.save(paciente);
    }

    //BORRAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id)
    {
        pacientesDao.deleteById(id);
    }

    //BUSCAR POR ID
    @Override
    @Transactional(readOnly=true)
    public pacientes findById(Integer id)
    {
        return pacientesDao.findById(id).orElse(null);
    }

    //BUSCAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<pacientes> findAll()
    {
        return (List<pacientes>) pacientesDao.findAll();
    }
    //BUSCAR POR numero de afiliado
    @Override
    @Transactional(readOnly=true)
    public ArrayList<pacientes> findByNumeroAfiliado(String numeroAfiliado)
    {
        return pacientesDao.findByNumeroAfiliado(numeroAfiliado);
    }
}