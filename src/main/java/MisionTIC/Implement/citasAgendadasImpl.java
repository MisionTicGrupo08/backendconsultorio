package MisionTIC.Implement;
import MisionTIC.Dao.citasAgendadasDao;
import MisionTIC.Models.citasAgendadas;
import MisionTIC.Services.citasAgendadasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class citasAgendadasImpl implements citasAgendadasService
{
    @Autowired
    private citasAgendadasDao citasAgendadasDao;
    
    //AGREGAR - ACTUALIZAR
    @Override
    @Transactional(readOnly=false)
    public citasAgendadas save(citasAgendadas citasAgendada)
    {
        return citasAgendadasDao.save(citasAgendada);
    }

    //BORRAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id)
    {
        citasAgendadasDao.deleteById(id);
    }

    //BUSCAR POR ID
    @Override
    @Transactional(readOnly=true)
    public citasAgendadas findById(Integer id)
    {
        return citasAgendadasDao.findById(id).orElse(null);
    }

    //BUSCAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<citasAgendadas> findAll()
    {
        return (List<citasAgendadas>) citasAgendadasDao.findAll();
    }
    //login
    
}