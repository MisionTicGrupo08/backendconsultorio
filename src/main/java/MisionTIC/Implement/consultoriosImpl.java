package MisionTIC.Implement;
import MisionTIC.Models.consultorios;
import MisionTIC.Dao.consultoriosDao;
import MisionTIC.Services.consultoriosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class consultoriosImpl implements consultoriosService{
    @Autowired
    private consultoriosDao administradoresDao;
    
    //AGREGAR - ACTUALIZAR
    @Override
    @Transactional(readOnly=false)
    public consultorios save(consultorios consultorio)
    {
        return administradoresDao.save(consultorio);
    }

    //BORRAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer IdConsultorio)
    {
        administradoresDao.deleteById(IdConsultorio);
    }

    //BUSCAR POR ID
    @Override
    @Transactional(readOnly=true)
    public consultorios findById(Integer IdConsultorio)
    {
        return administradoresDao.findById(IdConsultorio).orElse(null);
    }

    //BUSCAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<consultorios> findAll()
    {
        return (List<consultorios>) administradoresDao.findAll();
    }
}
