/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MisionTIC.Implement;

import MisionTIC.Models.administradores;
import MisionTIC.Dao.administradoresDao;
import MisionTIC.Services.administradoresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author USUARIO
 */
@Service
public class administradoresImpl implements administradoresService{

     @Autowired
    private administradoresDao administradoresDao;
    
    //AGREGAR - ACTUALIZAR
    @Override
    @Transactional(readOnly=false)
    public administradores save(administradores administradores)
    {
        return administradoresDao.save(administradores);
    }

    //BORRAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer IdAdministradores)
    {
        administradoresDao.deleteById(IdAdministradores);
    }

    //BUSCAR POR ID
    @Override
    @Transactional(readOnly=true)
    public administradores findById(Integer IdAdministradores)
    {
        return administradoresDao.findById(IdAdministradores).orElse(null);
    }

    //BUSCAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<administradores> findAll()
    {
        return (List<administradores>) administradoresDao.findAll();
    }
    //login
    @Override
    @Transactional(readOnly=true)
    public ArrayList<administradores> findByUsuario(String usuario)
    {
        return administradoresDao.findByUsuario(usuario);
    }
}
