package MisionTIC.Implement;

import MisionTIC.Models.citas;
import MisionTIC.Dao.citasDao;
import MisionTIC.Services.citasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
/**
 *
 * @author USUARIO
 */
@Service
public class citasImpl implements citasService{
    @Autowired
    private citasDao citasDao;
    
    //AGREGAR - ACTUALIZAR
    @Override
    @Transactional(readOnly=false)
    public citas save(citas citas)
    {
        return citasDao.save(citas);
    }

    //BORRAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer IdCita)
    {
        citasDao.deleteById(IdCita);
    }

    //BUSCAR POR ID
    @Override
    @Transactional(readOnly=true)
    public citas findById(Integer IdCita)
    {
        return citasDao.findById(IdCita).orElse(null);
    }

    //BUSCAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<citas> findAll()
    {
        return (List<citas>) citasDao.findAll();
    } 
}
