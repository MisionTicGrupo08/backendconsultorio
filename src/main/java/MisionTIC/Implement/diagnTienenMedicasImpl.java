package MisionTIC.Implement;
import MisionTIC.Dao.diagnTienenMedicasDao;
import MisionTIC.Models.diagnTienenMedicas;
import MisionTIC.Services.diagnTienenMedicasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class diagnTienenMedicasImpl implements diagnTienenMedicasService
{
    @Autowired
    private diagnTienenMedicasDao diagnTienenMedicasDao;
    
    //AGREGAR - ACTUALIZAR
    @Override
    @Transactional(readOnly=false)
    public diagnTienenMedicas save(diagnTienenMedicas diagnTienenMedica)
    {
        return diagnTienenMedicasDao.save(diagnTienenMedica);
    }

    //BORRAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id)
    {
        diagnTienenMedicasDao.deleteById(id);
    }

    //BUSCAR POR ID
    @Override
    @Transactional(readOnly=true)
    public diagnTienenMedicas findById(Integer id)
    {
        return diagnTienenMedicasDao.findById(id).orElse(null);
    }

    //BUSCAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<diagnTienenMedicas> findAll()
    {
        return (List<diagnTienenMedicas>) diagnTienenMedicasDao.findAll();
    }

}