/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MisionTIC.Implement;

import MisionTIC.Models.medicos;
import MisionTIC.Dao.medicosDao;
import MisionTIC.Services.medicosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author USUARIO
 */
@Service
public class medicosImpl implements medicosService{

    @Autowired
    private medicosDao medicosDao;

//AGREGAR - ACTUALIZAR
    @Override
    @Transactional(readOnly=false)
    public medicos save(medicos medicos)
    {
        return medicosDao.save(medicos);
    }

    //BORRAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer IdMedico)
    {
        medicosDao.deleteById(IdMedico);
    }

    //BUSCAR POR ID
    @Override
    @Transactional(readOnly=true)
    public medicos findById(Integer IdMedico)
    {
        return medicosDao.findById(IdMedico).orElse(null);
    }

    //BUSCAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<medicos> findAll()
    {
        return (List<medicos>) medicosDao.findAll();
    }

    //login
    @Override
    @Transactional(readOnly=true)
    public ArrayList<medicos> findByCedula(Integer cedula)
    {
        return medicosDao.findByCedula(cedula);
    }
    
}
