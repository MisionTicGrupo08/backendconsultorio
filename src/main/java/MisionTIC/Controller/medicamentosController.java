
package MisionTIC.Controller;

import MisionTIC.Models.medicamentos;
import MisionTIC.Services.medicamentosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/medicamentos")
public class medicamentosController 
{
    @Autowired
    private medicamentosService medicamentosService;

//AGREGAR
    @PutMapping(value="/")
    public ResponseEntity<medicamentos> agregar(@RequestBody medicamentos medicamento)
    {
        medicamentos obj = medicamentosService.save(medicamento);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

//EDITAR
    @PostMapping(value="/")
    public ResponseEntity<medicamentos> editar(@RequestBody medicamentos medicamento)
    {
        medicamentos obj = medicamentosService.findById(medicamento.getIdMedicamento());
        if(obj != null)
        {
            obj.setNombre(medicamento.getNombre());
            obj.setPresentacion(medicamento.getPresentacion());
            obj.setPresentacion(medicamento.getPresentacion());
            medicamentosService.save(obj);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    }

//BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<medicamentos> eliminar(@PathVariable Integer id)
    {
        medicamentos obj = medicamentosService.findById(id);
        if(obj != null)
        {
            medicamentosService.delete(id);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    }
//BUSCAR POR ID
    @GetMapping("/list/{id}")
    public medicamentos buscarId(@PathVariable Integer id)
    {
        return medicamentosService.findById(id);
    }
//BUSCAR todo   
    @GetMapping("/list")
    public List<medicamentos> buscar()
    {
        return medicamentosService.findAll();
    }
}
