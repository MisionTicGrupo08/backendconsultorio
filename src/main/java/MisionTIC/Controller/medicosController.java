/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MisionTIC.Controller;

import MisionTIC.Models.medicos;
import MisionTIC.Services.medicosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
/**
 *
 * @author USUARIO
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/medicos")
public class medicosController {
        @Autowired
    private medicosService medicosservices;
 //AGREGAR
    @PutMapping(value="/")
    public ResponseEntity<medicos> agregar(@RequestBody medicos medico)
    {
        medicos obj = medicosservices.save(medico);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

 //EDITAR
    @PostMapping(value="/")
    public ResponseEntity<medicos> editar(@RequestBody medicos medico)
    {
        medicos obj = medicosservices.findById(medico.getIdMedico());
        if(obj != null)
        {
            obj.setIdConsultorio(medico.getIdConsultorio());
            obj.setNombre(medico.getNombre());
            obj.setApellido(medico.getApellido());
            obj.setCedula(medico.getCedula());
            obj.setGenero(medico.getGenero());
            obj.setFechaNacimiento(medico.getFechaNacimiento());
            obj.setCelular(medico.getCelular());
            obj.setClave(medico.getClave());
            obj.setEstado(medico.getEstado());
            medicosservices.save(obj);/*Esta linea puede ser la solucion*/
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    }
//BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<medicos> eliminar(@PathVariable Integer id)
    {
        medicos obj = medicosservices.findById(id);
        if(obj != null)
        {
            medicosservices.delete(id);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    }
//BUSCAR POR ID
    @GetMapping("/list/{id}")
    public medicos buscarId(@PathVariable Integer id)
    {
        return medicosservices.findById(id);
    }
//BUSCAR todo
    @GetMapping("/list")
    public List<medicos> buscar()
    {
        return medicosservices.findAll();
    }

    //login
    @GetMapping("/login")
    public List<medicos> login(@RequestParam("cedula") Integer cedula)
    {
        return medicosservices.findByCedula(cedula);
    }
}
