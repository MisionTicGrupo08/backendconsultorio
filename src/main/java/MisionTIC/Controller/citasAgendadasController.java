package MisionTIC.Controller;

import MisionTIC.Models.citasAgendadas;
import MisionTIC.Services.citasAgendadasService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/citasAgendadas")
public class citasAgendadasController
{
    @Autowired
    private citasAgendadasService citasAgendadasservice;

    //AGREGAR
    @PutMapping(value="/")
    public ResponseEntity<citasAgendadas> agregar(@RequestBody citasAgendadas citasAgendada)
    {
        citasAgendadas obj = citasAgendadasservice.save(citasAgendada);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    //EDITAR
    @PostMapping(value="/")
    public ResponseEntity<citasAgendadas> editar(@RequestBody citasAgendadas citasAgendada)
    {
        citasAgendadas obj = citasAgendadasservice.findById(citasAgendada.getIdcitasAgendada());
        if(obj != null)
        {
            obj.setIdCita(citasAgendada.getIdCita());
            obj.setIdPaciente(citasAgendada.getIdPaciente());
            obj.setMotivoCita(citasAgendada.getMotivoCita());
            citasAgendadasservice.save(obj);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<citasAgendadas> eliminar(@PathVariable Integer id)
    {
        citasAgendadas obj = citasAgendadasservice.findById(id);
        if(obj != null)
        {
            citasAgendadasservice.delete(id);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    //BUSCAR POR ID
    @GetMapping("/list/{id}")
    public citasAgendadas buscarId(@PathVariable Integer id)
    {
        return citasAgendadasservice.findById(id);
    }

    //BUSCAR all
    @GetMapping("/list")
    public List<citasAgendadas> buscar()
    {
        return citasAgendadasservice.findAll();
    }
       
}


