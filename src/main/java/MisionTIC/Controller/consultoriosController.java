package MisionTIC.Controller;

import MisionTIC.Models.consultorios;
import MisionTIC.Services.consultoriosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/consultorios")
public class consultoriosController 
{
    @Autowired
    private consultoriosService consultoriosService;

//AGREGAR
    @PutMapping(value="/")
    public ResponseEntity<consultorios> agregar(@RequestBody consultorios consultorio)
    {
        consultorios obj = consultoriosService.save(consultorio);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

//EDITAR
    @PostMapping(value="/")
    public ResponseEntity<consultorios> editar(@RequestBody consultorios consultorio)
    {
        consultorios obj = consultoriosService.findById(consultorio.getIdConsultorio());
        if(obj != null)
        {
            obj.setNumeroConsultorio(consultorio.getNumeroConsultorio());
            obj.setEstado(consultorio.getEstado());
            
            consultoriosService.save(obj);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    }

//BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<consultorios> eliminar(@PathVariable Integer id)
    {
        consultorios obj = consultoriosService.findById(id);
        if(obj != null)
        {
            consultoriosService.delete(id);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    }
//BUSCAR POR ID
    @GetMapping("/list/{id}")
    public consultorios buscarId(@PathVariable Integer id)
    {
        return consultoriosService.findById(id);
    }
//BUSCAR todo 
    @GetMapping("/list")
    public List<consultorios> buscar()
    {
        return consultoriosService.findAll();
    }
}
