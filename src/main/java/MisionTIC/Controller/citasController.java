package MisionTIC.Controller;

import MisionTIC.Models.citas;
import MisionTIC.Services.citasService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/citas")
public class citasController {

    @Autowired
    private citasService citasService;
//AGREGAR
    @PutMapping(value="/")
    public ResponseEntity<citas> agregar(@RequestBody citas cita)
    {
        citas obj = citasService.save(cita);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    //EDITAR
    @PostMapping(value="/")
    public ResponseEntity<citas> editar(@RequestBody citas cita)
    {
        citas obj = citasService.findById(cita.getIdCita());
        if(obj != null)
        {
            obj.setIdMedico(cita.getIdMedico());
            obj.setFecha(cita.getFecha());
            obj.setEstado(cita.getEstado());
            
            citasService.save(obj);/*Esta linea puede ser la solucion*/
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    }

//BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<citas> eliminar(@PathVariable Integer id)
    {
        citas obj = citasService.findById(id);
        if(obj != null)
        {
            citasService.delete(id);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    }
//BUSCAR POR ID
    @GetMapping("list/{id}")
    public citas buscarId(@PathVariable Integer id)
    {
        return citasService.findById(id);
    }
//BUSCAR todo
    @GetMapping("/list")
    public List<citas> buscar()
    {
        return citasService.findAll();
    }
}
