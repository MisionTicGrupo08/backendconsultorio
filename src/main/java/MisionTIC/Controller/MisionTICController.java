package MisionTIC.Controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping("/consultorioOnline")
public class MisionTICController
{
    @GetMapping("/")
    public ResponseEntity<Object> index()
    {
        return ResponseEntity.ok("¡Bienvenidos al consultorio online MINTIC!");
    }

}
