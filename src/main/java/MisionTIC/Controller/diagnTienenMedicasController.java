package MisionTIC.Controller;

import MisionTIC.Models.diagnTienenMedicas;
import MisionTIC.Services.diagnTienenMedicasService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/diagnTienenMedicas")
public class diagnTienenMedicasController 
{
    @Autowired
    private diagnTienenMedicasService diagnTienenMedicasService;

//AGREGAR
    @PutMapping(value="/")
    public ResponseEntity<diagnTienenMedicas> agregar(@RequestBody diagnTienenMedicas diagnTienenMedica)
    {
        diagnTienenMedicas obj = diagnTienenMedicasService.save(diagnTienenMedica);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

//EDITAR
    @PostMapping(value="/")
    public ResponseEntity<diagnTienenMedicas> editar(@RequestBody diagnTienenMedicas diagnTienenMedica)
    {
        diagnTienenMedicas obj = diagnTienenMedicasService.findById(diagnTienenMedica.getIdDiagnTienenMedica());
        if(obj != null)
        {
            obj.setIdDiagnostico(diagnTienenMedica.getIdDiagnostico());
            obj.setIdMedicamento(diagnTienenMedica.getIdMedicamento());
            obj.setCantidadMedicamento(diagnTienenMedica.getCantidadMedicamento());
            obj.setIndicaciones(diagnTienenMedica.getIndicaciones());
            
            diagnTienenMedicasService.save(obj);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    }

//BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<diagnTienenMedicas> eliminar(@PathVariable Integer id)
    {
        diagnTienenMedicas obj = diagnTienenMedicasService.findById(id);
        if(obj != null)
        {
            diagnTienenMedicasService.delete(id);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    }
//BUSCAR POR ID
    @GetMapping("/list/{id}")
    public diagnTienenMedicas buscarId(@PathVariable Integer id)
    {
        return diagnTienenMedicasService.findById(id);
    }
//BUSCAR todo 
    @GetMapping("/list")
    public List<diagnTienenMedicas> buscar()
    {
        return diagnTienenMedicasService.findAll();
    }
}