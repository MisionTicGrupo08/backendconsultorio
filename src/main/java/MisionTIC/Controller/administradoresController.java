package MisionTIC.Controller;

import MisionTIC.Models.administradores;
import MisionTIC.Services.administradoresService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/administradores")
public class administradoresController
{

    @Autowired
    private administradoresService administradoresservice;
    
    //AGREGAR
    @PutMapping(value="/")
    public ResponseEntity<administradores> agregar(@RequestBody administradores administrador)
    {
        administradores obj = administradoresservice.save(administrador);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<administradores> eliminar(@PathVariable Integer id){
    administradores obj = administradoresservice.findById(id);
    if(obj!=null)
        administradoresservice.delete(id);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //EDITAR
    @PostMapping(value="/")
    public ResponseEntity<administradores> editar(@RequestBody administradores administrador)
    {
        administradores obj = administradoresservice.findById(administrador.getIdAdministrador());
        if(obj!=null)
        {
            obj.setUsuario(administrador.getUsuario());
            obj.setNombre(administrador.getNombre());
            obj.setApellido(administrador.getApellido());
            obj.setUsuario(administrador.getUsuario());
            obj.setClave(administrador.getClave());
            administradoresservice.save(obj);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BUSCAR TODOS
    @GetMapping("/list")
    public List<administradores> consultarTodo()
    {
        return administradoresservice.findAll();
    }

    //BUSCAR POR ID
    @GetMapping("/list/{id}")
    public administradores consultaPorId(@PathVariable Integer id)
    {
        return administradoresservice.findById(id);
    }

    //login
    @GetMapping("/login")
    public List<administradores> login(@RequestParam("usuario") String usuario)
    {
        return administradoresservice.findByUsuario(usuario);
    }    
}
