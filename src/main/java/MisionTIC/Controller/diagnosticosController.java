package MisionTIC.Controller;

import MisionTIC.Models.diagnosticos;
import MisionTIC.Services.diagnosticosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/diagnosticos")
public class diagnosticosController
{
    @Autowired
    private diagnosticosService diagnosticosservice;

    //AGREGAR
    @PutMapping(value="/")
    public ResponseEntity<diagnosticos> agregar(@RequestBody diagnosticos diagnostico)
    {
        diagnosticos obj = diagnosticosservice.save(diagnostico);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    //EDITAR
    @PostMapping(value="/")
    public ResponseEntity<diagnosticos> editar(@RequestBody diagnosticos diagnostico)
    {
        diagnosticos obj = diagnosticosservice.findById(diagnostico.getIdDiagnostico());
        if(obj != null)
        {
            obj.setIdCitaAgendada(diagnostico.getIdCitaAgendada());
            obj.setDiagnostico(diagnostico.getDiagnostico());
            diagnosticosservice.save(obj);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<diagnosticos> eliminar(@PathVariable Integer id)
    {
        diagnosticos obj = diagnosticosservice.findById(id);
        if(obj != null)
        {
            diagnosticosservice.delete(id);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    //BUSCAR POR ID
    @GetMapping("/list/{id}")
    public diagnosticos buscarId(@PathVariable Integer id)
    {
        return diagnosticosservice.findById(id);
    }

    //BUSCAR all
    @GetMapping("/list")
    public List<diagnosticos> buscar()
    {
        return diagnosticosservice.findAll();
    }
    
}


