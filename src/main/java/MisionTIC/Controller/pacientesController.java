package MisionTIC.Controller;

import MisionTIC.Models.pacientes;
import MisionTIC.Services.pacientesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/pacientes")
public class pacientesController
{
    @Autowired
    private pacientesService pacientesservice;

    //AGREGAR
    @PutMapping(value="/")
    public ResponseEntity<pacientes> agregar(@RequestBody pacientes paciente)
    {
        pacientes obj = pacientesservice.save(paciente);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    //EDITAR
    @PostMapping(value="/")
    public ResponseEntity<pacientes> editar(@RequestBody pacientes paciente)
    {
        pacientes obj = pacientesservice.findById(paciente.getIdPaciente());
        if(obj != null)
        {
            obj.setNombre(paciente.getNombre());
            obj.setApellido(paciente.getApellido());
            obj.setCedula(paciente.getCedula());
            obj.setGenero(paciente.getGenero());
            obj.setFechaNacimiento(paciente.getFechaNacimiento());
            obj.setCelular(paciente.getCelular());
            obj.setClave(paciente.getClave());
            obj.setEstado(paciente.getEstado());
            pacientesservice.save(obj);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<pacientes> eliminar(@PathVariable Integer id)
    {
        pacientes obj = pacientesservice.findById(id);
        if(obj != null)
        {
            pacientesservice.delete(id);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    //BUSCAR POR ID
    @GetMapping("/list/{id}")
    public pacientes buscarId(@PathVariable Integer id)
    {
        return pacientesservice.findById(id);
    }

    //BUSCAR all
    @GetMapping("/list")
    public List<pacientes> buscar()
    {
        return pacientesservice.findAll();
    }

    //BUSCAR POR NUMERO DE AFILIADO
    @GetMapping("/login")
    public List<pacientes> login(@RequestParam("numeroAfiliado") String numeroAfiliado)
    {
        return pacientesservice.findByNumeroAfiliado(numeroAfiliado);
    }
}


