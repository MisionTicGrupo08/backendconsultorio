package MisionTIC.Dao;
import MisionTIC.Models.medicos;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
public interface medicosDao extends CrudRepository<medicos,Integer>{
    public abstract ArrayList<medicos> findByCedula(Integer cedula); 
}

