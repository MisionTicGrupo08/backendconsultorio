package MisionTIC.Dao;
import MisionTIC.Models.pacientes;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
public interface pacientesDao extends CrudRepository<pacientes,Integer>{
    public abstract ArrayList<pacientes> findByNumeroAfiliado(String numeroAfiliado); 
}