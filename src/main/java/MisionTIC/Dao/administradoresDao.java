package MisionTIC.Dao;
import MisionTIC.Models.administradores;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
public interface administradoresDao extends CrudRepository<administradores,Integer>{
    public abstract ArrayList<administradores> findByUsuario(String usuario);
}
