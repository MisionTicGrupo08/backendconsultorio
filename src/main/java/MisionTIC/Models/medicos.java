   //Llave foranea
    //PRIMARY KEY(IdMedico),
    //FOREIGN KEY(IdConsultorio) REFERENCES consultorios(IdConsultorio)
    //) ENGINE=InnoDB;
package MisionTIC.Models;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;
import javax.persistence.Temporal;

@Entity
@Table(name="medicos")
public class medicos implements Serializable{
   @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)

    @Column(name="idmedico")
    private int IdMedico;

    /*Esta es la llave foranea*/
    @ManyToOne
    @JoinColumn(name="idconsultorio")
    private consultorios IdConsultorio;
    
    public consultorios getIdConsultorio()
    {
        return IdConsultorio;
    }
    public void setIdConsultorio(consultorios IdConsultorio)
    {
        this.IdConsultorio = IdConsultorio;
    }
/*Aqui termina la llave foranea*/

    @Column(name="nombre")
    private String Nombre;

    @Column(name="apellido")
    private String Apellido;

    @Column(name="cedula")
    private int cedula;

    @Column(name="genero")
    private String Genero;

    @Column(name="fechanacimiento")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date FechaNacimiento;

    @Column(name="celular")
    private int Celular;

    @Column(name="clave")
    private String clave;

    @Column(name="estado")
    private int Estado;
 

    public medicos(int IdMedico, consultorios IdConsultorio, String Nombre, String Apellido, int Cedula, String Genero, Date FechaNacimiento, int Celular, String clave, int Estado) {
        this.IdMedico = IdMedico;
        this.IdConsultorio = IdConsultorio;
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.cedula = Cedula;
        this.Genero = Genero;
        this.FechaNacimiento = FechaNacimiento;
        this.Celular = Celular;
        this.clave = clave;
        this.Estado = Estado;
    }

    public medicos() {
    }

    public int getIdMedico() {
        return IdMedico;
    }

    public void setIdMedico(int IdMedico) {
        this.IdMedico = IdMedico;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int Cedula) {
        this.cedula = Cedula;
    }

    public String getGenero() {
        return Genero;
    }

    public void setGenero(String Genero) {
        this.Genero = Genero;
    }

    public Date getFechaNacimiento() {
        return FechaNacimiento;
    }

    public void setFechaNacimiento(Date FechaNacimiento) {
        this.FechaNacimiento = FechaNacimiento;
    }

    public int getCelular() {
        return Celular;
    }

    public void setCelular(int Celular) {
        this.Celular = Celular;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String Clave) {
        this.clave = Clave;
    }

    public int getEstado() {
        return Estado;
    }

    public void setEstado(int Estado) {
        this.Estado = Estado;
    }


}
