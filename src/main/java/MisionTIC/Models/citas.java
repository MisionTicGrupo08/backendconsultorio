/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MisionTIC.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.util.Date;

/**
 *
 * @author USUARIO
 */

@Entity
@Table(name="citas")
public class citas implements Serializable
{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="idcita")
    private int IdCita;

    @ManyToOne
    @JoinColumn(name="idmedico")
    private medicos IdMedico;

    @Column(name="fecha")
    private Date Fecha;

    @Column(name="estado")
    private int Estado;

    public citas() {
    }

    public citas(int IdCita, medicos IdMedico, Date Fecha, int Estado) {
        this.IdCita = IdCita;
        this.IdMedico = IdMedico;
        this.Fecha = Fecha;
        this.Estado = Estado;
    }

    public int getIdCita() {
        return IdCita;
    }

    public void setIdCita(int IdCita) {
        this.IdCita = IdCita;
    }

    public medicos getIdMedico() {
        return IdMedico;
    }

    public void setIdMedico(medicos IdMedico) {
        this.IdMedico = IdMedico;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date Fecha) {
        this.Fecha = Fecha;
    }

    public int getEstado() {
        return Estado;
    }

    public void setEstado(int Estado) {
        this.Estado = Estado;
    }


}
