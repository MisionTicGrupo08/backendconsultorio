package MisionTIC.Models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name="pacientes")
public class pacientes implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="idpaciente")
    private Integer IdPaciente;
    @Column(name="numeroafiliado")
    private String numeroAfiliado;
    @Column(name="nombre")
    private String Nombre;
    @Column(name="apellido")
    private String Apellido;
    @Column(name="cedula")
    private Integer Cedula;
    @Column(name="genero")
    private String Genero;
    @Column(name="fechanacimiento")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date FechaNacimiento;
    @Column(name="celular")
    private Integer Celular;
    @Column(name="clave")
    private String Clave;    
    @Column(name="estado")
    private Integer Estado;
    
    public pacientes(Integer IdPaciente, String numeroAfiliado, String Nombre, String Apellido, Integer Cedula, String Genero, Date FechaNacimiento, Integer Celular, String Clave, Integer Estado) {
        this.IdPaciente = IdPaciente;
        this.numeroAfiliado = numeroAfiliado;
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.Cedula = Cedula;
        this.Genero = Genero;
        this.FechaNacimiento = FechaNacimiento;
        this.Celular = Celular;
        this.Clave = Clave;
        this.Estado = Estado;
    }

    public pacientes() {
    }

    public Integer getIdPaciente() {
        return IdPaciente;
    }

    public String getNumeroAfiliado() {
        return numeroAfiliado;
    }

    public String getNombre() {
        return Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public Integer getCedula() {
        return Cedula;
    }

    public String getGenero() {
        return Genero;
    }

    public Date getFechaNacimiento() {
        return FechaNacimiento;
    }

    public Integer getCelular() {
        return Celular;
    }

    public String getClave() {
        return Clave;
    }

    public Integer getEstado() {
        return Estado;
    }

    public void setIdPaciente(Integer IdPaciente) {
        this.IdPaciente = IdPaciente;
    }

    public void setNumeroAfiliado(String NumeroAfiliado) {
        this.numeroAfiliado = NumeroAfiliado;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public void setCedula(Integer Cedula) {
        this.Cedula = Cedula;
    }

    public void setGenero(String Genero) {
        this.Genero = Genero;
    }

    public void setFechaNacimiento(Date FechaNacimiento) {
        this.FechaNacimiento = FechaNacimiento;
    }

    public void setCelular(Integer Celular) {
        this.Celular = Celular;
    }

    public void setClave(String Clave) {
        this.Clave = Clave;
    }

    public void setEstado(Integer Estado) {
        this.Estado = Estado;
    }
}
