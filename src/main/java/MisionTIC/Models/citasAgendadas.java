package MisionTIC.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="citasagendadas")
public class citasAgendadas implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="idcitaagendada")
    private Integer IdcitaAgendada;
    @ManyToOne
    @JoinColumn(name="idpaciente")
    private pacientes IdPaciente;
    @ManyToOne
    @JoinColumn(name="idcita")
    private citas IdCita;
    @Column(name="motivocita")
    private String MotivoCita;

    public citasAgendadas(Integer IdcitaAgendada, pacientes IdPaciente, citas IdCita, String MotivoCita) {
        this.IdcitaAgendada = IdcitaAgendada;
        this.IdPaciente = IdPaciente;
        this.IdCita = IdCita;
        this.MotivoCita = MotivoCita;
    }

    public citasAgendadas() {
    }

    public Integer getIdcitasAgendada() {
        return IdcitaAgendada;
    }

    public pacientes getIdPaciente() {
        return IdPaciente;
    }

    public citas getIdCita() {
        return IdCita;
    }

    public String getMotivoCita() {
        return MotivoCita;
    }

    public void setIdcitasAgendada(Integer IdcitasAgendada) {
        this.IdcitaAgendada = IdcitasAgendada;
    }

    public void setIdPaciente(pacientes IdPaciente) {
        this.IdPaciente = IdPaciente;
    }

    public void setIdCita(citas IdCita) {
        this.IdCita = IdCita;
    }

    public void setMotivoCita(String MotivoCita) {
        this.MotivoCita = MotivoCita;
    }
    
}
