
package MisionTIC.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 *
 * @author ANDRESCUADROS
 */
@Entity
@Table(name="medicamentos")
public class medicamentos implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="idmedicamento")
    private Integer IdMedicamento;
    @Column(name="nombre")
    private String Nombre;
    @Column(name="presentacion")
    private String Presentacion;
    @Column(name="estado")
    private Integer Estado;

    public medicamentos(Integer IdMedicamento, String Nombre, String Presentacion, Integer Estado) {
        this.IdMedicamento = IdMedicamento;
        this.Nombre = Nombre;
        this.Presentacion = Presentacion;
        this.Estado = Estado;
    }

    public medicamentos() {
    }

    public Integer getIdMedicamento() {
        return IdMedicamento;
    }

    public String getNombre() {
        return Nombre;
    }

    public String getPresentacion() {
        return Presentacion;
    }

    public Integer getEstado() {
        return Estado;
    }

    public void setIdMedicamento(Integer IdMedicamento) {
        this.IdMedicamento = IdMedicamento;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public void setPresentacion(String Presentacion) {
        this.Presentacion = Presentacion;
    }

    public void setEstado(Integer Estado) {
        this.Estado = Estado;
    }
    
}
