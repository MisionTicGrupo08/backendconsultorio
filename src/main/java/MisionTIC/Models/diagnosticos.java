package MisionTIC.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="diagnosticos")
public class diagnosticos implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="iddiagnostico")
    private Integer IdDiagnostico;
    @ManyToOne
    @JoinColumn(name="idcitaagendada")
    private citasAgendadas IdCitaAgendada;
    @Column(name="diagnostico")
    private String Diagnostico;

    public diagnosticos(Integer IdDiagnostico, citasAgendadas IdCitaAgendada, String Diagnostico) {
        this.IdDiagnostico = IdDiagnostico;
        this.IdCitaAgendada = IdCitaAgendada;
        this.Diagnostico = Diagnostico;
    }

    public diagnosticos() {
    }

    public Integer getIdDiagnostico() {
        return IdDiagnostico;
    }

    public citasAgendadas getIdCitaAgendada() {
        return IdCitaAgendada;
    }

    public String getDiagnostico() {
        return Diagnostico;
    }

    public void setIdDiagnostico(Integer IdDiagnostico) {
        this.IdDiagnostico = IdDiagnostico;
    }

    public void setIdCitaAgendada(citasAgendadas IdCitaAgendada) {
        this.IdCitaAgendada = IdCitaAgendada;
    }

    public void setDiagnostico(String Diagnostico) {
        this.Diagnostico = Diagnostico;
    }
}
