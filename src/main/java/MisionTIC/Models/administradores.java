package MisionTIC.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 *
 * @author USUARIO
 */
@Entity
@Table(name="administradores")
public class administradores implements Serializable
{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="idadministrador")
    private int IdAdministrador;

    @Column(name="nombre")
    private String Nombre;

    @Column(name="apellido")
    private String Apellido;

    @Column(name="usuario")
    private String usuario;

    @Column(name="clave")
    private String clave;
//PRIMARY KEY(IdAdministrador)
//) ENGINE=InnoDB; 

    public administradores() {
    }

    public administradores(int IdAdministrador, String Nombre, String Apellido, String Usuario, String Clave) {
        this.IdAdministrador = IdAdministrador;
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.usuario = Usuario;
        this.clave = Clave;
    }

    public int getIdAdministrador() {
        return IdAdministrador;
    }

    public void setIdAdministrador(int IdAdministrador) {
        this.IdAdministrador = IdAdministrador;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String Usuario) {
        this.usuario = Usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String Clave) {
        this.clave = Clave;
    }

}
