package MisionTIC.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="consultorios")
public class consultorios implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="idconsultorio")
    private int IdConsultorio;
    @Column(name="numeroconsultorio")
    private String NumeroConsultorio;
    @Column(name="estado")
    private int Estado;

//PRIMARY KEY(IdConsultorio)
//) ENGINE=InnoDB;

    public consultorios() {
    }

    public consultorios(int IdConsultorio, String NumeroConsultorio, int Estado) {
        this.IdConsultorio = IdConsultorio;
        this.NumeroConsultorio = NumeroConsultorio;
        this.Estado = Estado;
    }

    public int getIdConsultorio() {
        return IdConsultorio;
    }

    public void setIdConsultorio(int IdConsultorio) {
        this.IdConsultorio = IdConsultorio;
    }

    public String getNumeroConsultorio() {
        return NumeroConsultorio;
    }

    public void setNumeroConsultorio(String NumeroConsultorio) {
        this.NumeroConsultorio = NumeroConsultorio;
    }

    public int getEstado() {
        return Estado;
    }

    public void setEstado(int Estado) {
        this.Estado = Estado;
    }


}
