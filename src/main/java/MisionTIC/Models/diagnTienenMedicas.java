package MisionTIC.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="diagntienenmedicas")
public class diagnTienenMedicas implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="iddiagntienenmedica")
    private Integer IdDiagnTienenMedica;
    @ManyToOne
    @JoinColumn(name="iddiagnostico")
    private diagnosticos IdDiagnostico;
    @ManyToOne
    @JoinColumn(name="idmedicamento")
    private medicamentos IdMedicamento;
    @Column(name="cantidadmedicamento")
    private double CantidadMedicamento;
    @Column(name="indicaciones")
    private String Indicaciones;

    public diagnTienenMedicas(Integer IdDiagnTienenMedica, diagnosticos IdDiagnostico, medicamentos IdMedicamento, double CantidadMedicamento, String Indicaciones) {
        this.IdDiagnTienenMedica = IdDiagnTienenMedica;
        this.IdDiagnostico = IdDiagnostico;
        this.IdMedicamento = IdMedicamento;
        this.CantidadMedicamento = CantidadMedicamento;
        this.Indicaciones = Indicaciones;
    }

    public diagnTienenMedicas() {
    }

    public Integer getIdDiagnTienenMedica() {
        return IdDiagnTienenMedica;
    }

    public diagnosticos getIdDiagnostico() {
        return IdDiagnostico;
    }

    public medicamentos getIdMedicamento() {
        return IdMedicamento;
    }

    public double getCantidadMedicamento() {
        return CantidadMedicamento;
    }

    public String getIndicaciones() {
        return Indicaciones;
    }

    public void setIdDiagnTienenMedica(Integer IdDiagnTienenMedica) {
        this.IdDiagnTienenMedica = IdDiagnTienenMedica;
    }

    public void setIdDiagnostico(diagnosticos IdDiagnostico) {
        this.IdDiagnostico = IdDiagnostico;
    }

    public void setIdMedicamento(medicamentos IdMedicamento) {
        this.IdMedicamento = IdMedicamento;
    }

    public void setCantidadMedicamento(double CantidadMedicamento) {
        this.CantidadMedicamento = CantidadMedicamento;
    }

    public void setIndicaciones(String Indicaciones) {
        this.Indicaciones = Indicaciones;
    }
    
}
